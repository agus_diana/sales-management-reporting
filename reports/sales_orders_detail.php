<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once('../class/tcpdf/tcpdf.php');
include_once("../class/PHPJasperXML.inc.php");
include_once('../setting.php');

$par_bulan = $_GET["month"];
$par_tahun = $_GET["year"];

$parameters = array(
    "TAHUN"=>$par_tahun,
    "BULAN"=>$par_bulan
);

$PHPJasperXML = new PHPJasperXML();
//$PHPJasperXML->debugsql=true;
$PHPJasperXML->arrayParameter=$parameters;
$PHPJasperXML->load_xml_file("../reportfiles/sales_order_detail.jrxml");

$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db);
$PHPJasperXML->outpage("I");    //page output method I:standard output  D:Download file


?>
